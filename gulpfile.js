var child      = require('child_process');
var fs         = require('fs');
var gulp       = require('gulp');
var watch      = require('gulp-watch');
var rimraf     = require('rimraf');
var	less       = require('gulp-less');
var minifyCSS  = require('gulp-minify-css');
var exec       = require('child_process').exec;
var nodemon    = require('gulp-nodemon');
var babel      = require("gulp-babel");
var concat     = require("gulp-concat");

var env = process.env.NODE_ENV || 'development';


//clean old build
gulp.task('clean', function (cb) {  
  rimraf.sync('./public');
  cb();
});

//start development server
gulp.task('default', ['build'], function() {
  nodemon({
    script: 'index.js',
    env: { 'NODE_ENV': env }
  });  
  gulp.start('watch');
});


//build tasks
gulp.task('build', ['clean', 'index', 'less', 'css', 'jsx', 'js', 'img', 'fonts']);
gulp.task('index', function () {
	return gulp.src('app/index.html')
		   .pipe(gulp.dest('public'));
});
gulp.task('less', function() {
    return gulp.src('app/css/*.less')
        .pipe(less())
        .pipe(minifyCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('public/css'));
});
gulp.task('js', function() {
    return gulp.src('app/js/*.js')
        .pipe(gulp.dest('public/js'));
});
gulp.task('jsx', function() {
    return gulp.src('app/js/components/*.js')
        .pipe(babel())
        .pipe(concat('app.js'))
        .pipe(gulp.dest('public/js/'));
});
gulp.task('css', function() {
    return gulp.src('app/css/vendor.css')
        .pipe(gulp.dest('public/css'));
});
gulp.task('img', function() {
    return gulp.src('app/img/**/*')
        .pipe(gulp.dest('public/img'));
});
gulp.task('fonts', function() {
    return gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('public/fonts'));
});

//watch files for changes
gulp.task('watch', function() {
    watch('app/css/**/*.less',function(){
      gulp.start('less');
    });
    watch('app/js/*.js',function(){
      gulp.start('js');
    });
    watch('app/js/components/*.js',function(){
      gulp.start('jsx');
    });
    watch('app/index.html', function () {
      gulp.start('index');
    });
});
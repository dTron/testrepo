var Availability = React.createClass({
	
	getDefaultProps: function() {
		return {
			availability : {
				weekday_morning   : true,
				weekday_afternoon : false,
				weekday_evening   : false,
				weekend_morning   : true,
				weekend_afternoon : false,
				weekend_evening   : true,
			}
		};
	},

	getListItem : function (time, message) {
		var icon, 
			className = 'availability-item group';
		
		switch (time) {
			case 'morning' : 
				icon = <i className='fa fa-sun-o'></i>;
				break;
			case 'afternoon' :
				icon = <i className='fa fa-certificate'></i>;
				break;
			case 'evening' : 
				icon = <i className='fa fa-moon-o'></i>;
				break;
			default :
				break;
		}
		
		className = (message.indexOf('cannot') === -1) ? className+" active" : className;
		
		return (
			<li className={className} onClick={this.toggleAvailability} data-time={time}>
				<div className='box'>
					{icon}
					<span className='time'>{time}</span>
				</div>
				<div className='text'>{message}</div>
			</li>
		);
	},

	toggleAvailability : function (e) {
		var timeBox = e.currentTarget,
			list = timeBox.parentNode,
			availKey = list.getAttribute('type')+"_"+timeBox.getAttribute('data-time');

		var availability = this.props.availability;
		//toggle value of availabiliti
		availability[availKey] = !availability[availKey];
		this.setState({availability : availability});
	},

	render : function () {
		var weekday_list, weekend_list,
			times = ['morning','afternoon','evening'],
			self = this;

		weekday_list = times.map(function(time){
			var message;
			if ( self.props.availability['weekday_'+time] ) {
				message = 'I can work weekday ' + time + 's.'
			} else {
				message = 'I cannot work weekday ' + time + 's.'
			}
			return self.getListItem(time,message);
		});

		weekend_list = times.map(function(time){
			var message;
			if ( self.props.availability['weekend_'+time] ) {
				message = 'I can work weekend ' + time + 's.'
			} else {
				message = 'I cannot work weekend ' + time + 's.'
			}
			return self.getListItem(time,message);
		});
		
		return (
			<div className='availability card group'> 
				<div className='heading'>Availability</div>
				<ul className='availability-list' type='weekday'>
					<li className='list-header'>Weekdays</li>
					{weekday_list}
				</ul>
				<ul className='availability-list' type='weekend'>
					<li className='list-header'>Weekends</li>
					{weekend_list}
				</ul>
			</div>
		)
	}
});
var SubmittedApplications = React.createClass({
	
	getDefaultProps: function() {
		return {
			applications : [{
								id : 1,
								designation : 'Part Time Host',
								workplace : 'The Battery',
								distance : 0.3,
								status : 'Pending'
							},
							{
								id : 2,
								designation : 'Waiter',
								workplace : 'The Mousad',
								distance : 1.3,
								status : 'Pending'
							},
							{
								id : 3,
								designation : 'Host',
								workplace : '5A5',
								distance : 2.6,
								status : 'Pending'
							},
							{
								id : 4,
								designation : 'Host',
								workplace : 'Chiaroscuro',
								distance : 1.7,
								status : 'Pending'
							}
			]
		};
	},

	render : function () {
		var applications = this.props.applications.sort(function (app1, app2) {
			return app1.distance - app2.distance;
		}).map(function(app){
			return (
				<li className='application group'>
					<div className='designation'>
						<span className='post'>{app.designation}</span>
						<br/>
						{app.workplace}
					</div>
					<div className='distance'>
						{app.distance} miles away
					</div>
					<div className='status'>
						{app.status}
					</div>
				</li>
			);
		});

		return (
			<div className='submitted-applications card group'> 
				<div className='heading'>Submitted Applications</div>
				<ul className='applications-list'>{applications}</ul>
			</div>
		)
	}
});
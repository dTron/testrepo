var UserDetails = React.createClass({
	
	getDefaultProps: function() {
		return {
			userDetails : {
				name        : "Jonathan Dilyup",
				address     : "955 Pine Street, San Francisco, 94108",
				email       : "jonathan.dillyup@gmail.com",
				phoneNumber : null,
				profilePic  : "https://s3.amazonaws.com/uifaces/faces/twitter/kurafire/128.jpg"
			},
			templateConstants : {
				address : "Commuting from:",
				email   : "Email Address:",
				phone   : "Phone Number:"
			}

		};
	},

	render : function () {
		var phoneBlock;
		if (this.props.userDetails.phoneNumber) {
			phoneBlock = <div className='text'>{this.props.userDetails.phoneNumber}</div>;
		} else {
			phoneBlock = <button className='add-number'>Add Mobile Phone</button>;
		}
		return (
			<div className='user-details card group'> 
				<img src={this.props.userDetails.profilePic} className='profile-pic' />
				<div className='details-box'>
					<div className='name'>{this.props.userDetails.name}</div>
					<div className='detail-line'>
						<div className='subtext'>{this.props.templateConstants.address}</div>
						<div className='text'>{this.props.userDetails.address}</div>
					</div>
					<div className='detail-line'>
						<div className='subtext'>{this.props.templateConstants.email}</div>
						<div className='text'>{this.props.userDetails.email}</div>
					</div>
					<div className='detail-line'>
						<div className='subtext'>{this.props.templateConstants.phone}</div>
						{phoneBlock}
					</div>
				</div>
			</div>
		)
	}
});
var Reference = React.createClass({
	getDefaultProps: function() {
		return {
			references : [],
			errorMessage : 'You still need to add atleast one reference.',
			relations : [{
							id       : 1,
							name     : 'Manager',
							icon     : 'fa fa-group',
							selected : false
						},
						{
							id       : 2,
							name     : 'Owner',
							icon     : 'fa fa-user-plus',
							selected : false
						},
						{
							id       : 3,
							name     : 'Co-worker',
							icon     : 'fa fa-user',
							selected : false
						},
						{
							id       : 4,
							name     : 'Friend',
							icon     : 'fa fa-heart',
							selected : false
						}
			]
		};
	},
	getReferences : function () {
		//build reference list here and return
		return ;
	},
	getRelationItems : function () {
		var self = this;
		return this.props.relations.map(function (relation) {
			var className = relation.selected ? 'relation-item active' : 'relation-item';
			return (<li onClick={self.selectReferenceRelation} className={className} data-id={relation.id}>
				<div className='icon-box'><i className={relation.icon}></i></div>
				<div className='icon-name'>{relation.name}</div>
			</li>);
		});
	},
	selectReferenceRelation : function (e) {
		var relationBox = e.currentTarget;
		this.props.relations.forEach(function (relation) {
			if (relation.id.toString() === relationBox.getAttribute('data-id')) {
				relation.selected = !relation.selected;
			}
		});
		this.setState(this.props);
	},
	addReference : function (e) {
		console.log('submit stuff');
		e.preventDefault();
		return false;
	},
	getReferenceForm : function () {
		return (<div className='upload-section'>
			<button>Upload Resume</button>
			<div className='seperator'></div>
		</div>);
	},
	render : function () {
		var referenceSection;
		if ( !this.props.references.length ) {
			referenceSection = <div className='action-box'>
				<div className='action-required'>
					<i className='fa fa-exclamation-triangle warning-icon'></i>
					ACTION REQUIRED
				</div>
				<em className='message'>{this.props.errorMessage}</em>
			</div>
		} else {
			referenceSection = this.getReferences();
		}
		return (
			<div className='reference card group'> 
				<div className='heading'>References</div>
				<div className='form-box group'>
					{referenceSection}
					<form onSubmit={this.addReference} className='reference-form'>
						<div className='form-element-wrapper'>
							<div className='subtext'>What's your reference's name?</div>
							<input type='text' name='name' required placeholder='Full Name'/>
						</div>
						
						<div className='form-element-wrapper'>
							<div className='subtext'>What's their mobile phone number?</div>
							<input type='text' name='phone' required placeholder='Mobile Phone Number'/>
						</div>
						
						<div className='form-element-wrapper'>
							<div className='subtext'>What's their relationship with you?</div>
							<ul className='relations-list group'>
								{this.getRelationItems()}
							</ul>
						</div>
						<button type='submit'>Ask for Reference</button>
					</form>
				</div>
			</div>
		)
	}
});
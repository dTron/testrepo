var Experience = React.createClass({
	getDefaultProps: function() {
		return {
			experiences : [],
			errorMessage : 'You still need to add your work experience.'
		};
	},
	getExperiences : function () {
		//build experience list here and return
		return ;
	},
	addExperience : function (e) {
		console.log('submit stuff');
		e.preventDefault();
		return false;
	},
	getUploadSection : function () {
		return (<div className='upload-section'>
			<button>Upload Resume</button>
			<div className='seperator'></div>
			<form onSubmit={this.addExperience}>
				<div className='form-element-wrapper'>
					<div className='subtext'>Where did you work?</div>
					<input type='text' name='company_name' required placeholder='Company Name'/>
				</div>
				
				<div className='form-element-wrapper'>
					<div className='subtext'>What was your job?</div>
					<input type='text' name='position' required placeholder='Position'/>
				</div>
				
				<div className='form-element-wrapper'>
					<div className='subtext'>How long did you work there?</div>
					<select name='experience' required>
						<option selected="selected"> &lt; 1year </option>
						<option> 1-3 years </option>
						<option selected="selected"> &gt; 3 years </option>
					</select>
				</div>
				<button type='submit'>Add Experience</button>
			</form>
		</div>);
	},
	render : function () {
		var experienceSection;
		if ( !this.props.experiences.length ) {
			experienceSection = <div className='action-box'>
				<div className='action-required'>
					<i className='fa fa-exclamation-triangle warning-icon'></i>
					ACTION REQUIRED
				</div>
				<em className='message'>{this.props.errorMessage}</em>
			</div>
		} else {
			experienceSection = this.getExperiences();
		}
		return (
			<div className='experience card group'> 
				<div className='heading'>Work Experience</div>
				<div className='form-box group'>
					{experienceSection}
					{this.getUploadSection()}
				</div>
			</div>
		)
	}
});
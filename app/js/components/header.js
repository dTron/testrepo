var Header = React.createClass({
	render : function () {
		return (
			<header>
				<div className='content-wrapper group'>
					<div className='header-item logo'>INSTAWORK</div>
					<div className='header-item search-box'>
						<GeoSuggest />
					</div>
					<div className='header-item jobs'>Jobs</div>
					<div className='header-item profile'>Profile</div>
				</div>
			</header>
		)
	}
});
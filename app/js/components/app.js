var MainApp = React.createClass({
	render : function () {
		return (
			<div>
				<Header />
				<div className='content-wrapper group'>
					<div className='content-column content-column--half content-column--rightPad'>
						<UserDetails />
						<SubmittedApplications />
						<Availability />
					</div>
					<div className='content-column content-column--half content-column--leftPad'>
						<Experience />
						<Reference />
					</div>
				</div>
			</div>
		);
	}
});

window.onload = function () {
	React.render(<MainApp />, document.querySelector('#app-container'));	
}

